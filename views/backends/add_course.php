<?php 
	session_start();
 

	// Check if the user is logged in, if not then redirect to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    ?>
	    	<script type="text/javascript">
	    		window.open("../../admin/index.php")
	    	</script>
	   <?php 
	}

	include ('admin_header.php');
	
	include ('admin_header.php');
	// Include config file
	include('../../connection.php');
	$conn = Conn();


 ?>

 <!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		td{
			color:white;
		}
		tr{
			color:white;
		}
		a{
			color:yellow;
			text-decoration: none;
		}
		a:hover{
			text-decoration: none !important;
			color:red;
		}
	
		label{
			color:white;
		}


	</style>
</head>
<body>

	<main class="container">
		<h1 style="color:white;">Add new course</h1>

		<div style="margin-bottom: 10px;">
			<form action="process_create_course.php" method="post" enctype="multipart/form-data">

			  <div class="form-group">
			    <label for="coursetitle">Title</label>
			    <input type="text" class="form-control" id="coursetitle" name="coursetitle" placeholder="Title">
			  </div>

			  <div class="form-group">
			    <label for="price">Price</label>
			    <input type="number" class="form-control" id="price" name="price" placeholder="Price">
			  </div>
			  <div class="form-group">
			    <label for="fileToUpload">File input</label>
			    <input type="file" id="fileToUpload" name="fileToUpload">
			    <p class="help-block">Example block-level help text here.</p>
			  </div>
			  
			 <input type="submit" value="Upload Image" name="submit">
			</form>
		</div>
		
	</main>

</body>
</html>