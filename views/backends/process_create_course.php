<?php 
	include ('admin_header.php');
	// Include config file
	include('../../connection.php');
	//include('../../main/myfileupload.php');
	$conn = Conn();
 ?>

 <!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			color:white !important;
		}


	</style>
</head>
<body>
	<?php
		
		function myFileUpload()
		  {
		  
		      $target_dir = "../../media/";
		 
		      $target_file = $target_dir .basename($_FILES["fileToUpload"]["name"]);
		      $file_name = basename($_FILES["fileToUpload"]["name"]);


		
		      $uploadOk = 1;
		      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		     
		      // Check if image file is a actual image or fake image
		      if(isset($_POST["submit"])) {
		        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		        if($check !== false) {
		          echo "File is an image - " . $check["mime"] . ".";
		          $uploadOk = 1;
		        } else {
		          echo "File is not an image.";
		          $uploadOk = 0;
		        }
		      }
		     
		      // Check if file already exists
		      if (file_exists($target_file)) {
		        echo "Sorry, file already exists.";
		        $uploadOk = 0;
		      }

		      // Check file size
		      if ($file_size > 500000) {
		        echo "Sorry, your file is too large.";
		        $uploadOk = 0;
		      }
		      // Allow certain file formats
		      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		      && $imageFileType != "gif" ) {
		        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		        $uploadOk = 0;
		      }


		      // Check if $uploadOk is set to 0 by an error
		      if ($uploadOk == 0) {
		        echo "Sorry, your file was not uploaded.";
		        die();
		      // if everything is ok, try to upload file
		      } else {

		        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		          // echo "The file ". htmlspecialchars(basename($file_name)). " has been uploaded.";
		          return $file_name;
		        
		        } else {
		          echo "Sorry, there was an error uploading your file.";
		          die();
		        }
		      }
		  }

		
		

		$course_title =  $_POST['coursetitle'];
		$price = $_POST['price'];
		$image_file = myFileUpload();


		
	   
	?>
	<main class="container">
		<?php 
			$sql = "INSERT INTO tbl_course(title, price, course_image) VALUES ('$course_title', $price, '$image_file')";
			if ($conn->query($sql) === TRUE) {
		    ?>
			    <script type="text/javascript">
			    	alert("New record was saved.");
			    	window.open("course.php")
			    </script>
				
			<?php

			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}


		?>
	</main>

</body>
</html>