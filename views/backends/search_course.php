<?php 
	session_start();
 
	// Check if the user is logged in, if not then redirect to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    ?>
	    	<script type="text/javascript">
	    		window.open("../../admin/index.php")
	    	</script>
	   <?php 
	}

	include ('admin_header.php');
	// Include config file
	include('../../connection.php');
	$conn = Conn();
	$title = $_POST['txt_title'];
	

	$sql = "SELECT * FROM tbl_course WHERE title LIKE '%$title%'";

	
	$r_result = $conn->query($sql);

	?>
		<table class="table table-bordered" id="tbl_course">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Price</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						 if ($r_result->num_rows > 0) {
							while($row = $r_result->fetch_assoc()) { 
							?>
								<tr>
									<td>
										<span><?php echo $row['id'] ?></span>
									</td>
									<td><?php echo $row['title']; ?></td>
									<td><?php echo $row['price']; ?></td>
									<td><img src="../../media/<?php echo $row['course_image']; ?>" width=80; height=80; class="img-rounded"/></td>
									<td style="color:red;">
										<a href="edit_course.php?pk=<?php echo $row['id'] ?>"> <span class="glyphicon glyphicon-pencil"></span> Edit </a> |
										<a href="delete_course.php?pk=<?php echo $row['id'] ?>"> <span class="glyphicon glyphicon-trash"></span> Delete </a>
									</td>
								</tr>
							<?php			 
							}
						}

					?>
					
				</tbody>
			</table>

	<?php 


?>