<?php 


	// Start connect to database
	include('../../connection.php');
	$conn = Conn();

	$user_name = $_POST['username'];
	$password = $_POST['txt_password'];
    $confirm_password = $_POST['txt_confirm_password'];

    if ($password != $confirm_password)
    {
    	?>
    	<script type="text/javascript">
	    	alert("Password and confirm password is not equal.");
	    	window.open("create_user.php","_self");
	    </script>
	    
	<?php 
	die();
    }
   
	$h_password = password_hash($password, PASSWORD_DEFAULT);
	// $h_confirm_password = password_hash($confirm_password, PASSWORD_DEFAULT);

    $s_search = "SELECT * FROM tbl_user WHERE username = '$user_name'";
	$result_search = $conn->query($s_search);

	if ($result_search->num_rows > 0) {
		echo 'This username already exist, please change username.';
		die();
	}


	$sql = "INSERT INTO tbl_user (user_name, password) VALUES ('$user_name', '$h_password')";

	if ($conn->query($sql) === TRUE) {
    ?>
	    <script type="text/javascript">
	    	alert("Account user created successfully.");
	    	window.open("dashboard.php","_self")
	    </script>

		
	<?php

	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}

?>