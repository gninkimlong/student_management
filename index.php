<?php 

    include "connection.php";
    include "views/fronts/header.php";
    $conn = Conn();

    session_start();
    echo "User Name: " . $_SESSION["user_name"] . ".<br>";
    echo "Password: " . $_SESSION["pwd"] . ".";
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
</head>
<body>
	<style type="text/css">
		
		.card img{
			width: 100%;
			height: 150px;
			border-radius: 5px 5px;
		}
		.card {
			padding:5px 5px;
		}
	</style>
	<main style="background-color: white;">

		<?php 

			// $sql = "INSERT INTO tbl_course(id, title, price, course_image, active) VALUES (NULL, 'PHP Programing Language', 80, 'media/php.jpg', 1)";
			// $sql = "UPDATE tbl_course SET price = 90 WHERE id = 4";

			// if($result = $conn->query($sql)==True)
			// {

			// 	echo "You inserted one new record";
			// 	echo "<span style='color:white;'> Number of new udpated: ".mysql_affected_rows($conn)."<span>";
			// }else{
			// 	echo "You got an error";
			// 	var_dump($conn);
			// }
			$sqlString ="SELECT * FROM tbl_schedule as s 
				INNER JOIN tbl_course as c, 
				tbl_instructor as ins,
				tbl_shift as sh
				WHERE c.id = s.course_id AND 
				ins.id = s.instructor_id AND 
				sh.id = s.id";

				$result = $conn->query($sqlString);

				while($row = $result->fetch_assoc()) {	
				?>
				<div class="container"> 	
					<div class="row">
						<div class="col-md-3">
							<div class="card">
								<img src="<?php echo $row['course_image'] ?>" class="card-img-top" alt="...">
								<div class="card-body">
									<h5 class="card-title"><?php echo $row['title']; ?></h5>
										<p class="card-text">Price : $<?php echo $row['price'] ?></p>
										<p class="card-text">Time : 45 Hours</p>
										<a href="views/fronts/detail.php?pk=<?php echo $row['id'] ?>" class="btn btn-primary">Detail</a>
								</div>
							</div>
						</div>
				<?php 
					}
				?>
						
				
			</div>
		</div>
	</main>

	<?php 
		mysql_close($conn);
	?>
</body>
</html>
